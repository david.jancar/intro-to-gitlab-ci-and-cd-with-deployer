<?php declare (strict_types=1);

namespace LexiSoft\CiCD;

final class MyClass
{
	public static function shout(string $string): void
	{
		echo $string;
	}
}
