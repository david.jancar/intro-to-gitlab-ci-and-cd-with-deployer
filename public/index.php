<?php

use LexiSoft\CiCD\MyClass;

require __DIR__ . '/../vendor/autoload.php';

//MyClass::shout('Hello world!');

echo getenv('TEST') ?? '';

//$database = 'mydatabase';
//$password = 'mypassword';
//$user = 'myuser';
//$host = 'mysql';

$database = getenv('MYSQL_DATABASE') ?? '';
$password = getenv('MYSQL_ROOT_PASSWORD') ?? '';
$user = getenv('MYSQL_USER') ?? '';
$host = getenv('MYSQL_HOST') ?? '';

echo "database=$database\npassword=$password\nuser=$user\nhost=$host\n";

//$pdo = new PDO("mysql:dbname=$database;host=$host", $user, $password);
$pdo = new PDO("mysql:dbname=$database;host=$host", 'root', 'test');