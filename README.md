# Intro to Gitlab CI and CD with deployer

https://gitlab.com/lexisoft/intro-to-gitlab-ci-and-cd-with-deployer/tree/4-basic-deploy-options

## References
- [Deployer](https://deployer.org)
- [DPL](https://github.com/travis-ci/dpl)
- [Gitlab CI](https://about.gitlab.com/product/continuous-integration/)
    - [Leading CI platform](https://about.gitlab.com/2017/09/27/gitlab-leader-continuous-integration-forrester-wave/)
    - [Gitlab CI for Github](https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/github_integration.html)


Informace

CI Lint pro testovani gitlab-ci.yml napr. spatna definice stages

allow_failure: true (vychozi chovani je posilani mailu pri selhani stage, pomoci allow_failure true nam nebude posilat email, ale pokracuje se dal)

https://ohmyz.sh/

https://github.com/dg/ftp-deployment

https://deployer.org/

composer require deployer/deployer --dev

vytvoreni deploy.php:
v rootu pustit binarku php vendor/deployer/deployer/bin/dep init

samotny deploy:
C:\xampp\htdocs\intro-to-gitlab-ci-and-cd-with-deployer>php vendor/deployer/deployer/bin/dep deploy

cloud server:
https://cloud.digitalocean.com


pro deploy je potreba nasmerovat nginx na adresar current

