<?php
namespace Deployer;

require 'recipe/common.php';

// Project name
set('application', 'my_project');

// Project repository
set('repository', 'https://gitlab.com/david.jancar/intro-to-gitlab-ci-and-cd-with-deployer.git');

// Shared files/dirs between deploys
set('shared_files', []);
set('shared_dirs', []);

// Writable dirs by web server
set('writable_dirs', []);
set('allow_anonymous_stats', false);

task('deploy_to_my_server', [
    //1 zazipuj vse
    //2 nahraj na server
    //3 rozzipuj
]);

// Hosts

//host('192.168.8.53')
//    ->user('jancar')
//    ->set('deploy_path', '~/home/jancar/www/skoleni2');
//host('46.101.229.238')
//    ->user('skoleni')
//    ->set('deploy_path', '~/home/skoleni/jancar');
host('185.14.184.107')
    ->user('root')
    ->configFile('~/.ssh/config')
    ->identityFile('~/.ssh/id_rsa')
    ->forwardAgent(true)
    ->set('deploy_path', '~/home/skoleni/jancar');

// Tasks

desc('Deploy your project');
//task('deploy', [
//    'deploy:info',
//    'deploy:update_code'
//]);
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
//    'deploy:deploy_to_my_server',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
